package ru.zerezhka.dogs.ui.base

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.akaita.java.rxjava2debug.RxJava2Debug
import ru.zerezhka.dogs.R

open class BaseActivity : AppCompatActivity() {

    open fun navigateTo(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(getContainerResId(), fragment)
                .commitNow()
    }

    protected fun getContainerResId(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        RxJava2Debug.enableRxJava2AssemblyTracking()
    }
}
