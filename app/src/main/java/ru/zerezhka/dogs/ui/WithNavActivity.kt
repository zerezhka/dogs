package ru.zerezhka.dogs.ui

import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import ru.zerezhka.dogs.R
import ru.zerezhka.dogs.ui.about.AboutFragment
import ru.zerezhka.dogs.ui.base.BaseActivity
import ru.zerezhka.dogs.ui.main.MainFragment

const val NEED_TO_NAV = "NAVIGATE_STATE"

class MainActivity : BaseActivity() {

    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var mNavigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.with_navigation_drawer_activity)
        setupToolbar()
        setupDrawer()
        if (savedInstanceState == null) {
            navigateTo(MainFragment.newInstance())
        }
    }

    private fun setupDrawer() {
        mDrawerLayout = findViewById(R.id.drawer_layout)
        mNavigationView = findViewById(R.id.nav_view)

        mNavigationView.setCheckedItem(R.id.main_menu_item)
        mNavigationView.setNavigationItemSelectedListener {
            it.isChecked = true

            when (it.itemId) {
                R.id.main_menu_item -> {
                    navigateTo(MainFragment.newInstance())
                    true
                }
                R.id.about_menu_item -> {
                    navigateTo(AboutFragment.newInstance())
                    true
                }
                else -> false
            }
        }
    }

    private fun setupToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun navigateTo(fragment: Fragment) {
        super.navigateTo(fragment)
        mDrawerLayout.closeDrawers()
    }
}
