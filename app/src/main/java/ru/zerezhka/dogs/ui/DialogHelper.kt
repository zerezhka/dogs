package ru.zerezhka.dogs.ui

import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog

class DialogHelper {
    companion object {
        fun showOkButtonAlert(context: Context?, text: String) {
            context?.let {
                AlertDialog.Builder(context)
                        .setMessage(text)
                        .setPositiveButton("OK") { dialogInterface, i ->
                            dialogInterface.dismiss() //do nothing
                        }
                        .show()
            }
        }

        fun showOkButtonAlert(context: Context?, @StringRes text: Int) {
            context?.let {
                AlertDialog.Builder(context)
                        .setMessage(text)
                        .setPositiveButton("OK") { dialogInterface, i ->
                            dialogInterface.dismiss() //do nothing
                        }
                        .show()
            }
        }
    }
}