package ru.zerezhka.dogs.model;

import android.util.Log;

import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JSONHelper {
    public static final JSONArray fromJSONObject(JSONObject jsonObject, String key) throws JSONException {
        JSONObject asObj = jsonObject.getJSONObject(key);
        JSONArray resultArray = new JSONArray();
        Iterator jsonKeys = asObj.keys();
        while (jsonKeys.hasNext()){
            JSONObject resultArrayElement = new JSONObject();
            String currentKey = (String)jsonKeys.next();
            Log.d("JSON Array key", currentKey);
            resultArrayElement.put(currentKey, jsonKeys.next());
            resultArray.put(resultArrayElement);
        }

        return resultArray;
    }
    public static final List<Breed> parseBreeds(LinkedTreeMap<String, LinkedTreeMap> map){
        LinkedTreeMap body = map.get("message");
        Iterator<String> keySet = body.keySet().iterator();
        ArrayList<Breed> breeds = new ArrayList<>();
        while (keySet.hasNext()){
            String key = keySet.next();
            Breed breed = new Breed();
            breed.setBreedName(key);
            ArrayList value = (ArrayList)body.get(key);
            if (!value.isEmpty()){
                breed.setSubBreeds(value);
            }
            breeds.add(breed);
        }
        return breeds;
    }
}
