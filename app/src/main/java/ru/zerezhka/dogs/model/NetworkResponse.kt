package ru.zerezhka.dogs.model

import com.google.gson.annotations.SerializedName

class NetworkResponse<T> {
    @SerializedName("status")
    val status: String? = null
    @SerializedName("message")
    val data: T? = null
}
