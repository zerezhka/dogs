package ru.zerezhka.dogs.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.zerezhka.dogs.R
import ru.zerezhka.dogs.network.DogsApiManager
import ru.zerezhka.dogs.ui.DialogHelper

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var subscribe: Disposable

    lateinit var recycler: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        recycler = view.findViewById(R.id.list_of_breeds_recycler)
        recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dam = DogsApiManager()
        subscribe = dam.getAllBreeds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("OK_MainFragment", it.toString())
                    viewModel.breeds = it
                    recycler.adapter = BreedsRecyclerAdapter(it)
                },{
                    Log.d("ERR_MainFragment", it.localizedMessage, it)
                    DialogHelper.showOkButtonAlert(context, R.string.there_is_no_spoon)
                })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        subscribe.dispose()
        recycler.adapter = null
    }

}
