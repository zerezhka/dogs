package ru.zerezhka.dogs.ui.main

import androidx.lifecycle.ViewModel
import ru.zerezhka.dogs.model.Breed

class MainViewModel : ViewModel() {
    lateinit var breeds: List<Breed>
}
