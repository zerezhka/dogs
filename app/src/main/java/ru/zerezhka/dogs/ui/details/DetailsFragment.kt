package ru.zerezhka.dogs.ui.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.zerezhka.dogs.R
import ru.zerezhka.dogs.network.DogsApiManager
import ru.zerezhka.dogs.ui.DialogHelper.Companion.showOkButtonAlert
import ru.zerezhka.dogs.ui.main.MainViewModel

const val EXTRA_BREED_NAME = "EXTRA_BREED_NAME"
const val EXTRA_SUB_BREED_NAME = "EXTRA_SUB_BREED_NAME"
const val NAVIGATE_TO_DETAILS = "Details_fragment"
class DetailsFragment : Fragment() {
    companion object {
        fun newInstance(breedName: String, subBreed: String?): DetailsFragment {
            val fragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putString(EXTRA_BREED_NAME, breedName)
            bundle.putString(EXTRA_SUB_BREED_NAME, subBreed)
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var viewModel: MainViewModel
    lateinit var disposable: Disposable
    lateinit var recycler: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_details, container, false)
        //do some assignments with view
        recycler = view.findViewById(R.id.details_images_recycler)
        recycler.layoutManager = GridLayoutManager(context,3)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val apiManager = DogsApiManager()
        val breedName = arguments!!.getString(EXTRA_BREED_NAME)
        val subBreed: String? = arguments!!.getString(EXTRA_SUB_BREED_NAME)
        if (subBreed != null) {
            activity?.title = "$breedName > $subBreed"
            disposable = apiManager.getAllImagesOfBreed(breedName, subBreed)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        Log.d("ERR_Details", it.localizedMessage, it)
                        showOkButtonAlert(context, R.string.there_is_no_spoon)
                    }
                    .map {
                        recycler.adapter = ImagesRecyclerAdapter(it)
                        it
                    }
                    .flatMapIterable { s -> s }
                    .subscribe {
                        //                        Log.d("IMAGE_ON_BASE_MY_LORD", it)
                    }
        } else {
            activity?.title = breedName
            disposable = apiManager.getAllImagesOfBreed(breedName)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError {
                        Log.d("ERR_Details", it.localizedMessage, it)
                        showOkButtonAlert(context, R.string.there_is_no_spoon)
                    }
                    .map {
                        recycler.adapter = ImagesRecyclerAdapter(it)
                        it
                    }
                    .flatMapIterable { s -> s }
                    .subscribe {
                        //                        Log.d("IMAGE_ON_BASE_MY_LORD", it)
                    }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        disposable.dispose()
        recycler.adapter = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }
}