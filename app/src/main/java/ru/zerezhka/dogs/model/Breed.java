package ru.zerezhka.dogs.model;

import android.support.annotation.Nullable;

import java.util.List;

public class Breed {
    //region fields
    private String breedName;
    private List<String> subBreeds;

    //endregion fields
    //region methods
    @Nullable
    public List<String> getSubBreeds() {
        return subBreeds;
    }

    public void setSubBreeds(List<String> subBreeds) {
        this.subBreeds = subBreeds;
    }

    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }
    //endregion methods
}
