package ru.zerezhka.dogs.ui.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ru.zerezhka.dogs.R

class ImagesRecyclerAdapter(listOfImages: List<String>) : RecyclerView.Adapter<ImagesRecyclerAdapter.ImagesViewHolder>() {
    private var images: List<String> = listOfImages
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_image_item, parent, false)
        return ImagesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int) {
        holder.imageView.setImage(images[position])
    }

    inner class ImagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView = itemView.findViewById(R.id.image_view)
    }
}

private fun ImageView.setImage(url: String) {
    Picasso.get()
            .load(url)
            .into(this)
}
