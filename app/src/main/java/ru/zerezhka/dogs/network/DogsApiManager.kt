package ru.zerezhka.dogs.network

import android.util.Log
import com.google.gson.internal.LinkedTreeMap
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.zerezhka.dogs.model.Breed
import ru.zerezhka.dogs.model.JSONHelper
import java.net.UnknownHostException

class DogsApiManager {
    val API_BASE_URL = "https://dog.ceo/api/"

    private var loggingInterceptor: HttpLoggingInterceptor
    private var httpClient: OkHttpClient
    private var retrofit: Retrofit
    private var dogsApi: DogsApi

    init {
        //LOOOOOOOOL, hack for no crashing on "no network" {@see https://github.com/ReactiveX/RxJava/issues/5425, https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0#error-handling}
        RxJavaPlugins.setErrorHandler {
            if (it is UnknownHostException) {
                throw UndeliverableException(it)
            }
        }

        loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()
        retrofit = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(httpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        dogsApi = retrofit.create(DogsApi::class.java)
    }

    fun getAllBreeds(): Flowable<List<Breed>> {
        return dogsApi.getAllBreeds().observeOn(Schedulers.io())
                .to { s ->
                    Log.d("BREEAD_OBJECT!LOOKATME", s.toString())
                    s.map { JSONHelper.parseBreeds(it as LinkedTreeMap<String, LinkedTreeMap<Any, Any>>) }
                            .subscribeOn(Schedulers.io())
                }
                .subscribeOn(AndroidSchedulers.mainThread())
    }

    fun getAllImagesOfBreed(breedName: String): Flowable<List<String>> {
        return dogsApi.getListOfImagesByBreed(breedName)
                .map { response -> response.data }
    }

    fun getAllImagesOfBreed(breedName: String, subBreedName: String): Flowable<List<String>> {
        return dogsApi.getListOfImagesByBreed(breedName, subBreedName)
                .map { response -> response.data }
    }

}