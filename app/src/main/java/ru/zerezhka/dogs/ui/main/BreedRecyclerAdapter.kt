package ru.zerezhka.dogs.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.zerezhka.dogs.R
import ru.zerezhka.dogs.model.Breed
import ru.zerezhka.dogs.ui.NEED_TO_NAV
import ru.zerezhka.dogs.ui.SimpleActivity
import ru.zerezhka.dogs.ui.details.EXTRA_BREED_NAME
import ru.zerezhka.dogs.ui.details.EXTRA_SUB_BREED_NAME
import ru.zerezhka.dogs.ui.details.NAVIGATE_TO_DETAILS

class BreedsRecyclerAdapter(private val breeds: List<Breed>?) : RecyclerView.Adapter<BreedsRecyclerAdapter.BreedViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.breed_recycler_item, parent, false)
        return BreedViewHolder(view)
    }

    override fun onBindViewHolder(holder: BreedViewHolder, position: Int) {
        val breed = breeds!![position]
        holder.textView.text = breed.breedName
        holder.textView.setOnClickListener {
            val intent = Intent(it.context, SimpleActivity::class.java)
            intent.putExtra(NEED_TO_NAV, NAVIGATE_TO_DETAILS)
            intent.putExtra(EXTRA_BREED_NAME, breed.breedName)
            it.context.startActivity(intent)
        }
        if (breed.subBreeds != null && !breed.subBreeds!!.isEmpty()) {
            holder.subBreedsLayout.visibility = View.VISIBLE
            for (subBread in breed.subBreeds!!) {
                val textView = TextView(holder.itemView.context)
                textView.text = subBread
                textView.setOnClickListener {
                    val intent = Intent(it.context, SimpleActivity::class.java)
                    intent.putExtra(NEED_TO_NAV, NAVIGATE_TO_DETAILS)
                    intent.putExtra(EXTRA_BREED_NAME, breed.breedName)
                    intent.putExtra(EXTRA_SUB_BREED_NAME, subBread)
                    it.context.startActivity(intent)
                }
                holder.subBreedsLayout.addView(textView)
            }
        } else {
            holder.subBreedsLayout.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return breeds?.size ?: 0
    }

    inner class BreedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textView: TextView = itemView.findViewById(R.id.breed_name)
        var subBreedsLayout: LinearLayout = itemView.findViewById(R.id.sub_breeds_container)
    }
}