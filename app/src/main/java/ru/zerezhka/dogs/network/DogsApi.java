package ru.zerezhka.dogs.network;

import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.zerezhka.dogs.model.NetworkResponse;

public interface DogsApi {
    @GET("breeds/list/all")
    Flowable<LinkedTreeMap> getAllBreeds();
    @GET("breed/{breed}/images")
    Flowable<NetworkResponse<List<String>>> getListOfImagesByBreed(@Path("breed") String breed);

    @GET("breed/{breed}/{subBreed}/images")
    Flowable<NetworkResponse<List<String>>> getListOfImagesByBreed(@Path("breed") String breed, @Path("subBreed") String subBreed);
}
