package ru.zerezhka.dogs.ui

import android.os.Bundle
import ru.zerezhka.dogs.R
import ru.zerezhka.dogs.ui.base.BaseActivity
import ru.zerezhka.dogs.ui.details.DetailsFragment
import ru.zerezhka.dogs.ui.details.EXTRA_BREED_NAME
import ru.zerezhka.dogs.ui.details.EXTRA_SUB_BREED_NAME
import ru.zerezhka.dogs.ui.details.NAVIGATE_TO_DETAILS

class SimpleActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.simple_activity)
        val fragment = when (intent.getStringExtra(NEED_TO_NAV)) {
            NAVIGATE_TO_DETAILS -> DetailsFragment.newInstance(intent.getStringExtra(EXTRA_BREED_NAME), intent.getStringExtra(EXTRA_SUB_BREED_NAME))
            else -> {
                DetailsFragment()
            }
        }
        navigateTo(fragment)
    }
}